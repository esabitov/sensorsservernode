// const express = require('express')
// const app = express()
// //include controllers
// const api = require('./controllers')
// const err = require('./middleware/err')
// const config = require('./config')
// const logger = require('./util/logger')

// app.set('jwtSecret', config.secrets.secret)

// if (config.seed) require('./util/seed')
// require('./middleware/appMiddleware')(app)

// app.use('/', api)
// app.use(err)

// app.listen(config.port, config.ip, function(){
//   console.log(`listening on: ${config.port}`)
// })
// logger.log(`listening on: ${config.port}`)


// const WebSocket = require('ws');

// const wss = new WebSocket.Server({port: 8282});

// wss.on('connection', function connection(ws) {
//   ws.on('message', function incoming(message) {
//     console.log('received: %s', message);
//   });
//   ws.send('something');
// });


// const express = require('express');
// const http = require('http');
// const WebSocket = require('ws');

// const app = express();

// // app.use(function (req, res) {
// //   res.send({ msg: "hello" });
// // });

// // Add headers
// // app.use(function (req, res, next) {
// //   // Website you wish to allow to connect
// //   res.setHeader('Access-Control-Allow-Origin', '*');
// //   // Request methods you wish to allow
// //   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
// //   // Request headers you wish to allow
// //   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
// //   // Set to true if you need the website to include cookies in the requests sent
// //   // to the API (e.g. in case you use sessions)
// //   res.setHeader('Access-Control-Allow-Credentials', true);
// //   // Pass to next layer of middleware
// //   next();
// // });

// const server = http.createServer(app);
// const wss = new WebSocket.Server({ server });

// wss.on('connection', function connection(ws) {

//   ws.on('message', function incoming(message) {
//     console.log('received: %s', message);
//   });

//   ws.send('something');
// });

// server.listen(8080, function listening() {
//   console.log('Listening on %d', server.address().port);
// });



// let http = require('http')
// let express = require('express')
// let ws = require('ws')

// let app = express()

// // Add headers
// app.use(function (req, res, next) {
//   // Website you wish to allow to connect
//   res.setHeader('Access-Control-Allow-Origin', '*');
//   // Request methods you wish to allow
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//   // Request headers you wish to allow
//   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//   // Set to true if you need the website to include cookies in the requests sent
//   // to the API (e.g. in case you use sessions)
//   res.setHeader('Access-Control-Allow-Credentials', true);
//   // Pass to next layer of middleware
//   next();
// });

// let server = http.Server(app)
// let wss = new ws.Server({ server: server, path: '/', clientTracking: false, maxPayload: 1024 })

// let config = {
//   port: 8080,
//   wshost: 'ws://localhost:8080'
// }

// server.listen(8080)


//I know this is horrible because I am pausing for 60 seconds but docker was erroring out because mysql wasnt up when this connection was being made
//And i tried a bunch of things to try and set up a trail and error situation os that it tried to keep reconnecting but nothing works so I am giving up
//and just adding 60 second pause here
const mysql = require('mysql2');
const sleep = require('sleep');
const config = require('./config');
sleep.sleep(60);
const connection = mysql.createConnection({host:config.db.options.host,user:config.db.username,password:config.db.password});
connection.query('CREATE DATABASE ' + config.db.database, (err, results, fields)=>{
  console.log('could not CREATE DATABASE err:',err.code);
  // basically do nothing. This just makes sure that the database exists.
});
connection.end();



const express = require('express');
const http = require('http');
const url = require('url');
const WebSocket = require('ws');



const api = require('./controllers')
const err = require('./middleware/err')
const logger = require('./util/logger')

const app = express();

app.set('jwtSecret', config.secrets.secret)

if (config.seed) require('./util/seed')
require('./middleware/appMiddleware')(app)

app.use('/', api)
app.use(err)

const server = http.createServer();
server.on('request', app);
const wss = new WebSocket.Server({ server });

let sensors = []
let clients = []

wss.on('connection', function connection(ws) {
  //separate client from sensors
  if(ws.upgradeReq.url == '/?sensor=true'){ //sensor
    sensors.push(ws);
  }
  else{
    clients.push(ws);
  }
  // const location = url.parse(ws.upgradeReq.url, true);
  // You might use location.query.access_token to authenticate or share sessions 
  // or ws.upgradeReq.headers.cookie (see http://stackoverflow.com/a/16395220/151312) 

  //Messages will only come from sensors so just update the sensor with the cur time
  ws.on('message', function incoming(msg) {
    let data = JSON.parse(msg);
    console.log(data);
    if(data.action == 'updateSensorTime'){
      ws['sensorTime'] = data.time;
    }
    else if(data.action == 'restartSensor'){
      console.log(sensors[data.sensor_id]);
      try { 
        sensors[data.sensor_id].send(JSON.stringify({
          action:'restartSensor'
        }));
      }
      catch (e) { 
        console.log(e)
      }
    }
  });
  
  ws.on('error', (err)=>{
    console.log(err);
  });

  ws.on('close', (code, message)=>{
    //on websocket close if ws is a sensor then remove it from the sensors array
    var index = sensors.indexOf(ws);
    if(index > -1){
      sensors.splice(index, 1);
    }
  });

});


server.listen(config.port,'0.0.0.0',function listening() {
  console.log(server.address());
});


function checkTime(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

function sendAll() {
  if(clients.length > 0){
    for (var i=0; i<clients.length; i++) {
      var today = new Date();
      var h = today.getHours();
      var m = today.getMinutes();
      var s = today.getSeconds();
      // add a zero in front of numbers<10
      m = checkTime(m);
      s = checkTime(s);
      var time = h + ":" + m + ":" + s;
      data = {serverTime:time}
      data['sensors'] = []
      for(var x=0; x<sensors.length; x++){
        data['sensors'].push({
          id:x,
          sensorTime:sensors[x]['sensorTime']
        })
      }
      if(clients[i].readyState == 1){
        clients[i].send(JSON.stringify(data));
      }
    }
  }
  setTimeout(()=>{
    sendAll(); 
  },1000); 
}

sendAll();

