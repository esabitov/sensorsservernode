const mysql = require('mysql2');
// const sleep = require('sleep');
const Sequelize = require('sequelize');
const config = require('./config');

const db = new Sequelize(config.db.database, config.db.username, config.db.password, config.db.options);

module.exports = db;


