const Site = require('../../models/siteModel')
const request = require('request')

exports.params = (req, res, next, id) => {
  Site.findById(id)
    .then((site) => {
      req.site = site
      next()
    })
    .catch((err) => next(err))
}

exports.get = (req, res, next) => {
  Site.findAll({
    where: {
      sitegroup_id: req.params.siteGroupId
    }
  })
    .then((sites) => res.send(sites))
    .catch((err) => next(err))
}

exports.put = (req, res, next) => {
  Site.update(req.body, {
    fields: [ 'street_number', 'street', 'unit', 'city', 'state', 'zip', 'extended_zip', 'parcel', 'lat', 'lng' ],
    where: {id: req.params.id},
    returning: true, // need to this to get result[1].dataValues
    plain: true // need to this to get result[1].dataValues
  })
  .then((result) => {
    res.send(result[1].dataValues)
  })
  .catch((err) => next(err))
}

exports.post = (req, res, next) => {
  console.log('posting a site')
  let siteParams = Object.assign({}, req.body, {sitegroup_id: req.params.siteGroupId})
  Site.create(siteParams)
    .then((site) => res.send(site))
    .catch((err) => next(err))
}

exports.delete = (req, res, next) => {
  Site.destroy({
    where: {
      id: req.params.id
    }
  })
  .then((destroyedSite) => {
    res.send(req.params.id)
  })
  .catch((err) => {
    res.send(err)
  })
}
