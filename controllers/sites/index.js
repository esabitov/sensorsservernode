const express = require('express')
const router = express.Router({mergeParams: true})
const siteController = require('./siteController')

router.param('id', siteController.params)

router.route('/')
  .get(siteController.get)
  .post(siteController.post)

router.route('/:id')
  // .get(siteController.getOne)
  .put(siteController.put)
  .delete(siteController.delete)

module.exports = router
