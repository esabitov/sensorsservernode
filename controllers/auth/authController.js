const Auth = require('../../models/authModel')
const User = require('../../models/userModel')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

exports.login = (req, res, next) => {
  console.log('yo')
  let { email, password } = req.body

  User.findOne({
    where: {
      email: email
    }
  })
    .then((user) => {
      console.log('yo')
      if (!user){
        res.send( {success: false, message: 'user not found'} )
      } else {
        let auth = new Auth(user)
        bcrypt.compare(password, auth.user.password).then((authenticated) => {
          auth.authenticated = authenticated
          let token = auth.createTokenOrDenyAccess(req)
          res.send(token)
        })
      }
    }).catch((err) => {
      console.log(err)
      res.send(err)
    })
}

exports.verify = (req, res, next) => {
  const { token } = req.body
  const verification = jwt.verify(token, req.app.get('jwtSecret'))
  res.send(verification)
}
