const express = require('express')
const users = require('./user')
const sites = require('./sites')
const auth = require('./auth')
const authMiddleware = require('../middleware/authMiddleware')
const unlessSkip = require('../middleware/unlessSkip')

const router = express.Router()
const whitelistedRoutes = {
  '/users': 'POST',
  '/auth/login': 'POST',
  '/auth/verify': 'POST',
  '/ws': 'GET'
  // '/posts': 'GET',
  // '/posts/:id': 'GET'
}

router.use(unlessSkip(whitelistedRoutes, authMiddleware))

router.use('/auth', auth)
router.use('/users', users)
router.use('/sites', sites)

module.exports = router
