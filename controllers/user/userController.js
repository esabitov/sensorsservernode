const User = require('../../models/userModel')
const Auth = require('../../models/authModel')
const bcrypt = require('bcrypt')

exports.params = function(req, res, next, id) {
  User.findById(id)
    .then(function(user) {
      if (!user) {
        next(new Error('No user with that id'))
      } else {
        req.user = user
        next()
      }
    }, function (err) {
      next(err)
    })
}

exports.get = (req, res, next) => {
  User.findAll({
    order: [ ['createdAt', 'ASC'] ],
    attributes: ['id', 'email']
  }).then((users) => {
    res.send(users)
  }).catch((err) => {
    res.send(err)
  })
}

exports.getOne = (req, res, next) => {
  res.send(user)
}

exports.put = (req, res, next) => {
  let auth = new Auth()
  auth.createPassword(req.body.user.password).then((hashedPassword) => {
    User.update({
      email: req.body.user.email,
      password: hashedPassword
    }, {
      fields: [ 'email', 'password' ],
      where: {id: req.params.id},
      returning: true, // need to this to get result[1].dataValues
      plain: true // need to this to get result[1].dataValues
    })
    .then((result) => {
      // res.send(result[1].dataValues)
      res.send({success:true})
    })
    .catch((err) => next(err))
  })
  .catch((err) => {
    next(err)
  })
}

exports.create = (req, res, next) => {
  let auth = new Auth()
  auth.createPassword(req.body.user.password).then((hashedPassword) => {
    // temporary add organization id for now
    let userParams = Object.assign({}, req.body.user, {password: hashedPassword})
    User.create(userParams).then((user) => {
      // console.log('user created');
      // bcrypt.compare(req.body.password, user.password).then((authenticated) => {
      //   auth.user = user
      //   auth.authenticated = authenticated
      //   let token = auth.createTokenOrDenyAccess(req)
      //   res.send(token)
      // })
      res.send({id:user.id,email:user.email})
    }).catch((err) => {
      // console.log('err1',err.errors[0].message)
      res.send({success:false,message:err.errors[0].message})
      // next(err.errors[0].message)
    })
  })
  .catch((err) => {
    next(err)
  })
}

exports.delete = (req, res, next) => {
  //make sure we cannot destroy first user in db
  User.findOne({
    order: [ ['id', 'ASC'] ]
  }).then((first_user) => {
    // /
    if(req.user.id === first_user.dataValues.id){
      res.send({success:false,'message':'You cannot delete the first user.'})
    }else{
      User.destroy({
        where: {
          id: req.user.id
        }
      })
      .then((destroyedUser) => {
        res.send(req.user)
      })
      .catch((err) => {
        res.send(err)
      })
    }
  }).catch((err) => {
    res.send(err)
  })

  
}
