const config = require('../config');

let sequelizeConfig = {
  database:config.db.database,
  username:config.db.username,
  password:config.db.password,
  host: config.db.options.host,
};

sequelizeConfig.production = sequelizeConfig;
sequelizeConfig.test = sequelizeConfig;
sequelizeConfig.development = sequelizeConfig;

//Exports final database config
module.exports = sequelizeConfig;