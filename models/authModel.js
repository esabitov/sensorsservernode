const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const saltRounds = 10

class Auth {

  constructor (user = null) {
    this.authenticated = false
    this.user = user
  }

  createPassword (userProvidedPassword) {
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(saltRounds, (err, salt) => {
        bcrypt.hash(userProvidedPassword, salt, null)
        .then((hash) => {
          resolve(hash)
        }).catch((err) => {
          reject(err)
        })
      })
    })
  }

  createTokenOrDenyAccess (req) {
    if (!this.authenticated) {
      return { sucess: false, message: 'Auth failed, wrong password' }
    } else {
      let { id, email, admin } = this.user
      let userInfo = { id, email }
      if (admin) userInfo['scope'] = 'admin'
      var token = jwt.sign(userInfo, req.app.get('jwtSecret'), {})
      return { success: true, message: 'token created', token: token }
    }
  }
}

module.exports = Auth
