const Sequelize = require('sequelize')
const db = require('../db.js')

const Sensor = db.define('Sensor', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  value: Sequelize.DECIMAL(10, 8),
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE
  }
})

module.exports = Sensor
