var config = {
  env: process.env.NODE_ENV || 'development',
  logging: false,

  dev: 'development',
  test: 'testing',
  prod: 'production',
  secrets: {
    secret: 'testing',
    salt: 'salt-testing'
  },
  expireTime: 60 * 60 * 24 * 10,
  port: process.env.PORT || 8080,
  db: {
    database: process.env.DATABASE_NAME || 'sensors',
    username: process.env.DATABASE_USERNAME || 'root',
    password: process.env.DATABASE_PASSWORD || '',
    options: {
      host: process.env.DATABASE_HOST || 'localhost',
      dialect: 'mysql',
      logging: false,
      pool: {
        max: 5,
        min: 0,
        idle: 10000
      }
    }
  }
}

process.env.NODE_ENV = process.env.NODE_ENV || config.env
config.env = process.env.NODE_ENV

var envConfig = require(`${__dirname}/${config.env}`)
module.exports = Object.assign({}, config, envConfig || {})
