To run the seeder on the server side within docker:


```
#!command

docker exec -it container_name bash
cd to app dir
./node_modules/.bin/sequelize db:seed:all
```

To start without docker run from root dir:


```
#!command

node index.js
```

Digital ocean server setup:

- Spin up new Ubuntu 16 install
- Create new user with sudo privilages (follow steps 2, 3, and 4 from https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-14-04)
- install docker (```sudo apt install docker.io```)
- install docker compose (step 1 here https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-16-04)
- Add new user to docker group (```sudo usermod -aG docker $(whoami)```) logout and log back in